To build on Denovo, just run the ./build-*.sh script

To run:

> module purge
> module load PrgEnv-gnu/7.1 bzip2/1.0.6
> export PATH=${PATH}:`pwd`/../2.6.0/bin
> vsearch --help

 You can either just set your $PATH, as above, or follow the tutorial
instructions and build a module file for it.
