#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1
module load zlib/1.2.11
module load bzip2/1.0.6

vsn=2.6.0
file=v${vsn}.tar.gz
dir=vsearch-$vsn
url=https://github.com/torognes/vsearch/archive/v2.6.0.tar.gz

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir

./autogen.sh

  CPPFLAGS="$ZLIB_INC $BZIP2_INCLUDE" \
  LDFLAGS="$ZLIB_LIB -lz $BZIP2_LIB -lbz2" \
  ./configure --prefix=$PREFIX

make -j 16
make install

cd ..
rm -rf $dir
