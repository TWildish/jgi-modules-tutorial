#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1
module load perl/5.24.0

vsn=3.23
file=MUMmer$vsn.tar.gz
url=https://downloads.sourceforge.net/project/mummer/mummer/$vsn/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url

#
# Have to build in-place. Yeurck!
mkdir -p $PREFIX
tar --strip-components=1 --directory $PREFIX -xf $file
cd $PREFIX
make -j 16
make check
