#!/bin/bash

#
# Use 'set -ex' to echo the commands that are executed, and to
# abort execution on error. This makes debugging much easier.
#
# You can use 'set -e' and 'set -x' separately if you wish.
# 'set -e' is to stop on error
# 'set -x' is to echo the command to stdout
set -ex

#
# Make sure you're in the right directory. This lets the script
# be called from anywhere, it automatically goes to the directory
# the script is in.
cd `dirname $0`

#
# Always strictly control your environment. Remove all modules,
# then add only the ones you need to build the software.
#
# Always specify full version numbers, if you take defaults,
# and the default changes, you might not know why your new build
# fails while your old one worked. Worse, you won't know for sure
# what version of gcc etc was used to build the older version
#
# Always use dependencies from modules instead of from the operating
# system if you can, it's much more portable. Check the build
# instructions for your package to find out what the depenencies are
module purge
module load PrgEnv-gnu/7.1

#
# Specify the version only once. All being well, when a new version
# comes out, you can copy this file, change only this line, and
# run the script to build the new version.
vsn=912

#
# Use the version number to derive the name of the source-code, the
# directory it unpacks into, and the URL to download it from. This
# is package-specific, of course!
#
# This also documents where the code comes from, useful if you want
# to check back for newer versions, or for help compiling the code
file=last-$vsn.zip
dir=last-$vsn
url=http://last.cbrc.jp/$file

#
# Specify the installation directory relative to the current
# location in the filesystem. Don't hardwire a path, that will
# destroy any flexibility you have to experiment with the build
# script on the same or other systems
#
# This is a convention we use at NERSC, you may prefer something else
PREFIX=$(cd ..; pwd)/$vsn

#
# Wipe out any pre-existing installation so you get a fresh install
# This avoids having artifacts hang around from one build attempt
# to the next. Worse, your new build may fail but you might not notice,
# and keep using the old build
[ -d $PREFIX ] && rm -rf $PREFIX

#
# Download the software if you don't already have it locally. If it's
# already there, just use it, don't fetch it again
[ -f $file ] || wget -O $file $url

#
# Like $PREFIX, wipe out any temporary build directory that may be
# hanging around from previous build attempts
[ -d $dir ] && rm -rf $dir

#
# Unpack the source code, go into the build directory, build the
# software, and install it. This is highly package-specific, make
# sure you read the instructions carefully
unzip $file
cd $dir
#
# Note the '-j 16' to speed up builds
make -j 16 CXXFLAGS="-O3 -mtune=native -std=c++11 -pthread -DHAS_CXX_THREADS -static-libstdc++"
mkdir -p $PREFIX
make install prefix=$PREFIX

#
# After installing the software, remove the temporary build directory
cd ..
rm -rf $dir