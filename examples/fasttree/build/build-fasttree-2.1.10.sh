#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1

vsn=2.1.10
file=FastTree-$vsn.c
url=http://www.microbesonline.org/fasttree/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
mkdir -p $PREFIX/bin

#
# FastTree is a single-product package with a single source-code file.
# Just tell the compiler to build it in-place, there's nothing to clean up
gcc --static -O3 -finline-functions -funroll-loops -Wall -o $PREFIX/bin/FastTree $file -lm
