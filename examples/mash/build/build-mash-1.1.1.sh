#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/6.3 # fails with 7.1
module load capnproto/0.5.3
module load boost/1.63.0
module load zlib/1.2.11

vsn=1.1.1
file=v$vsn.tar.gz
url=https://github.com/marbl/Mash/archive/$file
dir=Mash-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir

export CXXFLAGS="$ZLIB_INC"
export LDFLAGS="$ZLIB_LIB"
./bootstrap.sh
./configure --prefix=$PREFIX \
  --with-boost=$BOOST_DIR \
  --with-capnp=$CAPNPROTO_DIR
make -j 16
make install

cd ..
rm -rf $dir
